from kivy.uix.boxlayout import BoxLayout
from kivy.uix.slider import Slider
from kivy.uix.togglebutton import ToggleButton
from kivy.uix.label import Label
from kivy.uix.carousel import Carousel

from widget_tools import *

class NASA_TLXScreen(BoxLayout):
	def __init__(self, factors, **kwargs):
		super(NASA_TLXScreen, self).__init__(spacing=10, **kwargs)

		self.questions = []
		for heading, text in factors.items():
			widget = NASA_TLXFactor(heading, text)
			self.questions.append(widget)
			self.add_widget(widget)

	def getValues(self):
		values = {}
		for q in self.questions:
			values[q.getName()] = q.getValue()

		return values

class NASA_TLXFactor(BoxLayout):
	def __init__(self, heading, description, **kwargs):
		super(NASA_TLXFactor, self).__init__(orientation='vertical', spacing=0, padding=10)
		self.name = heading

		self.heading = wrapped(Label(text="[b][size=20]%s[/size][/b]" % heading, markup=True, size_hint=(1, 0.05), valign='top', halign='center'))
		self.description = wrapped(Label(text="[size=17]%s[/size]" % description, markup=True, size_hint=(1, 0.15), valign='top', halign='center'))

		self.slider = HashedSlider(orientation='vertical', min=1, max=21, value=11, size_hint=(1, 0.70))

		self.add_widget(self.heading)
		self.add_widget(self.description)

		labels = ("Good", "Poor") if heading == "Performance" else ("High", "Low")
		self.add_widget(Label(text=labels[0], size_hint=(1, .05), valign='top'))
		self.add_widget(self.slider)
		self.add_widget(Label(text=labels[1], size_hint=(1, .05), valign='top'))

	def getName(self):
		return self.name

	def getValue(self):
		return self.slider.value

class FactorComparison(BoxLayout):
	def __init__(self, factor1, description1, factor2, description2, **kwargs):
		super(FactorComparison, self).__init__(orientation='vertical', spacing=25, padding=50, **kwargs)

		buttonContainer = BoxLayout(spacing=50, padding=(0, 25))
		self.buttons = []
		for text, description in { factor1: description1, factor2: description2 }.items():
			b = wrapped(
				FactorButton(
					text, description,
					group='%s vs %s' % (factor1, factor2),
					halign='center',
					valign='middle',
					padding=(120, 0),
				)
			)
			self.buttons.append(b)
			buttonContainer.add_widget(b)
			b.bind(state=self.proceedToNext)

		self.add_widget(wrapped(Label(text='[b][size=30]Select the item which represents the more important contributor to workload for the specific task you performed in this experiment[/size][/b]', markup=True, halign='center')))
		self.add_widget(buttonContainer)

		self.add_widget(Label(text=""))

	def proceedToNext(self, instance, value):
		if value == "down":
			carousel = climbTo(instance, Carousel)
			if carousel.index < len(carousel.slides) - 1:
				carousel.index += 1

class FactorButton(ToggleButton):
	def __init__(self, name, description, **kwargs):
		super(FactorButton, self).__init__(text="[b][size=24]%s[/size][/b]\n\n[size=16]%s[/size]" % (name, description), markup=True, **kwargs)
		self.name = name
		self.description = description
