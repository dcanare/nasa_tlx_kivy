from random import shuffle

import kivy
kivy.require('1.7.2')

from kivy.app import App

from kivy.uix.boxlayout import BoxLayout
from kivy.uix.gridlayout import GridLayout
from kivy.uix.label import Label
from kivy.uix.button import Button
from kivy.uix.carousel import Carousel
from kivy.uix.popup import Popup
from kivy.uix.textinput import TextInput

from kivy.uix.anchorlayout import AnchorLayout

from nasa_tlx_components import *
from widget_tools import *

class NASA_TLX(App):
	def build(self):
		self.factors = {
			"Mental Demand": "How much mental and perceptual activity was required? Was the task easy or demanding, simple or complex?",
			"Physical Demand": "How much physical activity was required? Was the task easy or demanding, slack or strenuous?",
			"Temporal Demand": "How much time pressure did you feel due to the pace at which the tasks or task elements occurred? Was the pace slow or rapid?",
			"Performance": "How successful were you in performing the task? How satisfied were you with your performance?",
			"Frustration": "How irritated, stressed, and annoyed versus content, relaxed, and complacent did you feel during the task?",
			"Effort": "How hard did you have to work (mentally and physically) to accomplish your level of performance?",
		}

		self.carousel = Carousel(scroll_distance=30)
		
		subjectInfoBox = GridLayout(cols=2, size_hint=(0.5, .1))

		self.subjectFields = {}
		lastField = None
		for f in ["Subject ID", "Task"]:
			self.subjectFields[f] = TabTextInput()
			subjectInfoBox.add_widget(wrapped(Label(text="%s : " % f, halign="right", valign="middle")))
			subjectInfoBox.add_widget(self.subjectFields[f])
			if lastField != None:
				lastField.setNext(self.subjectFields[f])
				self.subjectFields[f].setPrevious(lastField)
				
			lastField = self.subjectFields[f]
		self.subjectFields["Subject ID"].focus = True
		
		container = AnchorLayout(anchor_x='center', anchor_y='center')
		container.add_widget(subjectInfoBox)
		
		self.carousel.add_widget(self.wrapItUp(container, isFirst=True))
		
		self.mainScreen = NASA_TLXScreen(self.factors)
		self.carousel.add_widget(self.wrapItUp(self.mainScreen, isFirst=True))

		self.factorComparisons = []
		factorList = []
		for name, description in self.factors.items():
			factorList.append(name)

		for i in range(len(factorList)):
			for j in range(i+1, len(factorList)):
				factor1 = factorList[i]
				factor2 = factorList[j]

				self.factorComparisons.append(
					FactorComparison(factor1, self.factors[factor1], factor2, self.factors[factor2])
				)

		shuffle(self.factorComparisons)

		for i, w in enumerate(self.factorComparisons):
			self.carousel.add_widget(self.wrapItUp(w, isLast=(i == len(self.factorComparisons)-1)))

		return self.carousel

	def wrapItUp(self, w, isFirst=False, isLast=False):
		container = BoxLayout(orientation='vertical')
		container.add_widget(w)

		buttonBox = BoxLayout(size_hint_y=.1, padding=(50, 25), spacing=50)
		if isFirst:
			button = Label()
		else:
			button = Button(text="<~  Previous")
			button.bind(state=self.buttonClicked)
		buttonBox.add_widget(button)
		
		if isLast:
			button = Button(text="Finish")
		else:
			button = Button(text="Next  ~>")
			
		button.bind(state=self.buttonClicked)
		buttonBox.add_widget(button)
		
		container.add_widget(buttonBox)
		
		return container

	def validate(self):
		for comp in self.factorComparisons:
			found = False
			for b in comp.buttons:
				if b.state == "down":
					found = True
					break
					
			if not found:
				self.carousel.load_slide(comp.parent)
				return False
		
		return True

	def buttonClicked(self, instance, state):
		if state == "normal":
			carousel = climbTo(instance, Carousel)
			if instance.text == "Next  ~>":
				carousel.index += 1
			elif instance.text == "<~  Previous":
				carousel.index -= 1
			elif instance.text == "Finish":
				if not self.validate():
					text = "You're missing some data :(\n\nClick anywhere on this screen close this message and then complete the form to continue."
					p = Popup(
						title="",
						content=wrapped(Label(text="[b][size=24]%s[/size][/b]" % text, halign='center', valign='middle', markup=True, padding=(30, 30),)),
						size_hint=(.5, .5)
					)
					p.content.bind(on_touch_down=p.dismiss)
					p.open()
				else:
					self.stop()

def runAsMainProgram():
	screen = NASA_TLX()
	screen.run()

	headings = []
	values = []

	for field in sorted(screen.subjectFields.keys()):
		headings.append(field)
		values.append(screen.subjectFields[field].text)
	
	for factor, value in screen.mainScreen.getValues().items():
		headings.append(factor)
		values.append("%0.2f" % value)

	factorCounts = {}
	tmpHeadings = []
	tmpValues = []
	for comp in screen.factorComparisons:
		for b in comp.buttons:
			if b.state == "down":
				tmpHeadings.append(b.group)
				tmpValues.append(b.name)

				if not b.name in factorCounts:
					factorCounts[b.name] = 0
				factorCounts[b.name] += 1

	for factor in sorted(factorCounts.keys()):
		headings.append("%s-weight" % factor)
		values.append(str(factorCounts[factor]))

	headings.extend(tmpHeadings)
	values.extend(tmpValues)
	
	try:
		os.mkdir("data")
	except:
		pass
		
	with open("data/nasa_tlx.tab", "a") as dataFile:
		dataFile.write("\t".join(headings))
		dataFile.write("\n")
		dataFile.write("\t".join(values))
		dataFile.write("\n")

if __name__ == '__main__':
	runAsMainProgram()
