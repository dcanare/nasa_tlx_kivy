from kivy.uix.textinput import TextInput
from kivy.uix.slider import Slider

from kivy.graphics import *


def climbTo(widget, stopAtType):
	if isinstance(widget.parent, stopAtType):
		return widget.parent
	else:
		return climbTo(widget.parent, stopAtType)

def wrapped(widget):
	widget.bind(size=widget.setter("text_size"))
	return widget


'''
	From: http://stackoverflow.com/questions/12037379/tab-enter-and-other-keystrokes-handling-in-kivys-textinput-widgets
'''
class TabTextInput(TextInput):
	def __init__(self, *args, **kwargs):
		self.next = kwargs.pop('next', None)
		self.previous = kwargs.pop('next', None)
		
		super(TabTextInput, self).__init__(*args, **kwargs)

	def setNext(self, next):
		self.next = next

	def setPrevious(self, previous):
		self.previous = previous

	def _keyboard_on_key_down(self, window, keycode, text, modifiers):
		key, key_str = keycode
		if key in (9, 13):
			if "shift" in modifiers and  self.previous is not None:
				self.previous.focus = True
				self.previous.select_all()
			elif self.next is not None:
				self.next.focus = True
				self.next.select_all()
		else:
			super(TabTextInput, self)._keyboard_on_key_down(window, keycode, text, modifiers)


class HashedSlider(Slider):
	def __init__(self, **kwargs):
		super(HashedSlider, self).__init__(**kwargs)
		self.bind(size=self.update_rect)
		
		self.hashes = InstructionGroup()
		self.canvas.insert(0, self.hashes)
			
		
	def update_rect(self, instance, value):
		if self.step == 0:
			steps = (self.range[1] - self.range[0]) + 1
		else:
			steps = (self.range[1] - self.range[0]) / self.step + 1

		self.hashes.clear()
		self.hashes.add(Color(0.5, 0.5, 0.5))
		
		if self.orientation == "vertical":
			length = self.height + self.padding
			for i in range(int(steps)):
				if i % 2 == 1:
					hashLength = self.width / 8
				else:
					hashLength = self.width / 8 * 2
				
				hashOffset = (self.width-hashLength) / 2

				self.hashes.add(
					Rectangle(
						pos=(self.x + hashOffset, self.y + float(i)/steps * length + self.padding),
						size=(hashLength, 2)
					)
				)
		else:
			length = self.width + self.padding
			for i in range(int(steps)):
				if i % 2 == 1:
					hashLength = self.height / 8
				else:
					hashLength = self.height / 8 * 2
				
				hashOffset = (self.height-hashLength) / 2

				self.hashes.add(
					Rectangle(
						pos=(self.x + float(i)/steps * length + self.padding, self.y + hashOffset),
						size=(2, hashLength)
					)
				)
